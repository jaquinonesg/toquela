<div class="row index">
    <!-- scripts maps -->
    <script type="text/javascript">
        var base_url = "{$base_url}";
        {if $error == 'surgio_error'}
        $(document).ready(function() {
            var fun = function() {
                //loadPopupRegister();
            };
            component.messageAcept("Error de registro.", "Surgió un error al ingresar el usuario, por favor inténtelo de nuevo.", fun, "danger");
            component.replaceStrURL(base_url);
        });
        {/if}
        {if $error == 'error_permisos'}
        $(document).ready(function() {
            var fun = function() {
                //loadPopupRegister();
            };
            component.messageAcept("Permisos.", "No tiene permisos para ingresar a la sección.", fun, "danger");
            component.replaceStrURL(base_url);
        });
        {/if}
        {if $error == 'error_captcha'}
        $(document).ready(function() {
            var fun = function() {
                //loadPopupRegister();
            };
            component.messageAcept("Error de CAPTCHA.", "Surgió un error al comprobar el CAPTCHA, por favor inténtelo de nuevo.", fun, "danger");
            component.replaceStrURL(base_url);
        });
        {/if}
        {if $error == 'existe_usuario'}
        $(document).ready(function() {
            var fun = function() {
                //loadPopupRegister();
            };
            component.messageAcept("Error de registro.", "El email con el que intentó registrarse ya existe, por favor ingrese otro o inicie sesión.", fun, "danger");
            component.replaceStrURL(base_url);
        });
        {/if}
        {if $error == 'error_datos'}
        $(document).ready(function() {
            var fun = function() {
                //loadPopupRegister();
            };
            component.messageAcept("Error de registro.", "No se enviaron los datos correctamente, todos los datos son obligatorios para el registro, por favor inténtelo de nuevo.", fun, "danger");
            component.replaceStrURL(base_url);
        });
        {/if}
        {if $error == 'error_email'}
        $(document).ready(function() {
            var fun = function() {
                //loadPopupRegister();
            };
            component.messageAcept("Error de registro.", "El email ingresado no es válido, por favor inténtelo de nuevo.", fun, "danger");
            component.replaceStrURL(base_url);
        });
        {/if}
    </script>
    
    <script type="text/javascript" src="http://maps.google.com/maps/api/js?libraries=places&sensor=false&language=es"></script>
    <script type="text/javascript" src="{$_params.site}/views/registro/jsmaps/maps.js">
    </script>
    
    <link rel="stylesheet" href="http://code.jquery.com/ui/1.10.3/themes/smoothness/jquery-ui.css">
    <script src="{$_params.site}/public/js/vendor/jquery-ui.js"></script>
    <script type="text/javascript" src="{$_params.site}/views/registro/jsmaps/controlmaps.js"></script>
    

    <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 header-index">
        <div class="col-xs-12 col-sm-12 col-md-10 col-lg-10 col-md-offset-1 col-lg-offset-1 header-index-container">
        
            <div class="col-xs-12 col-sm-6 col-md-6 col-lg-6">
                <img class="img-responsive" style="margin: 2px auto" width="300px" src="{$_params.site}/public/img/Pelota_doble.png"/>
            </div>
            <div class="col-xs-12 col-sm-6 col-md-6 col-lg-6 contenido">
                
        </div>
        <div class="clear"></div>
    </div>
    <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 clear section-novedades" style="display: none;">
        <div class="col-xs-12 col-sm-12 col-md-10 col-lg-10 col-md-offset-1 col-lg-offset-1 novedades-contend">
            {*<div class="header-index-map">
            <div id="search-map" class="col-xs-12 col-sm-3 col-md-3 col-lg-3">
            <p class="text-verde">BUSCAR</p>
            <div>
            <select id="slc_ciudades" class="select-default">
            <option value="">Ciudades</option>
            {foreach from=$ciudades item=val}
            <option value="{$val->ciudad|utf8_encode}, {$val->depart|utf8_encode}, {$val->pais|utf8_encode}">{$val->ciudad|utf8_encode}</option>
            {/foreach}
            </select>    
            <!--select id="slc_departamentos" class="Select-">
            <option value="">Departamentos</option>
            {foreach from=$departamentos item=departamento}
            <option>{$departamento->name}</option>
            {/foreach}
            </select-->
            </div>
            </br>
            <div>
            <select id="slc_buscas" class="select-default">
            <option value=""> ¿Qué busca?</option>
            <option value="2"> Jugadores</option>                                   
            <option value="3"> Canchas</option>                                   
            <option value="1"> Torneos</option>                    
            <option value="4"> Partidos</option>                    
            <!--option value="4"> Blogs</option>                                   
            <option value="5"> Ciudades</option--> 
            </select>      	            
            </div>
            </br>
            <div>
            <input type="text" id="filtro" placeholder="Escriba lo que desee buscar"/>
            </div>
            </br>
            </div>
            <div id="mapaToquela" class="col-xs-12 col-sm-9 col-md-9 col-lg-9"></div>
            <input type="hidden" id="lat" value=""/>
            <input type="hidden" id="lon" value=""/>
            <input type="hidden" id="radio" value=""/>
            <input type="hidden" id="zoom" value=""/>
            </div>*}
            <div class="clear separator_vertical" style="height: 30px;"><br></div>
            <div class="text-center text-gray-dark" id="title-novedades"><p>NOVEDADES</p></div>
            <br>
            <div class="col-xs-12 col-sm-4 col-md-4 col-lg-4">
                <div class="novedad-presentacion">
                    <div class="titunovedad"><p>Liga de Fútbol de Bogotá.</p></div>
                    <div class="contend-imgnovedad">
                        <a class="previa" href="{$_params.site}/public/img/img_ejemplo/toquela1.jpg" target="_blank">
                            <img class="img-responsive" alt="Novedad 1" src="{$_params.site}/public/img/img_ejemplo/toquela1.jpg"/>
                        </a>
                    </div>
                </div>
                <br>
                <p>{$utilnovedades->getResumen("")}</p>
                <br>
                <div class="text-right">
                    <button type="button" class="btn btn_blue_light" style="width: 100px;">Ver mas</button>
                </div>
                <br>
            </div>
            <div class="col-xs-12 col-sm-4 col-md-4 col-lg-4">
                <div class="novedad-presentacion">
                    <div class="titunovedad"><p>Torneo Relámpago de Fútbol 5, Inscripciones abiertas!</p></div>
                    <div class="contend-imgnovedad">
                        <a class="previa" href="{$_params.site}/public/img/img_ejemplo/toquela2.jpg" target="_blank">
                            <img class="img-responsive" alt="Novedad 2" src="{$_params.site}/public/img/img_ejemplo/toquela2.jpg"/>
                        </a>
                    </div>
                </div>
                <br>
                <p>{$utilnovedades->getResumen("")}</p>
                <br>
                <div class="text-right">
                    <button type="button" class="btn btn_blue_light" style="width: 100px;">Ver mas</button>
                </div>
                <br>
            </div>
            <div class="col-xs-12 col-sm-4 col-md-4 col-lg-4">
                <div class="novedad-presentacion">
                    <div class="titunovedad"><p>Torneo Relámpago Fútbol 5.</p></div>
                    <div class="contend-imgnovedad">
                        <a class="previa" href="{$_params.site}/public/img/img_ejemplo/toquela3.jpg" target="_blank">
                            <img class="img-responsive" alt="Novedad 3" src="{$_params.site}/public/img/img_ejemplo/toquela3.jpg"/>
                        </a>
                    </div>
                </div>
                <br>
                <p>{$utilnovedades->getResumen("")}</p>
                <br>
                <div class="text-right">
                    <button type="button" class="btn btn_blue_light" style="width: 100px;">Ver mas</button>
                </div>
                <br>
            </div>
            <div class="clear"><br></div>
        </div>
        <div class="clear"></div>
    </div>
</div>
