<?php /* Smarty version Smarty-3.1.8, created on 2015-11-03 12:52:53
         compiled from "/var/www/lostorneos/views/noticias/sections/verNoticia.tpl" */ ?>
<?php /*%%SmartyHeaderCode:19466603785638f47592a590-31955474%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_valid = $_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    '08c351caeed800afcb797dab97b67f62945d1f57' => 
    array (
      0 => '/var/www/lostorneos/views/noticias/sections/verNoticia.tpl',
      1 => 1446565483,
      2 => 'file',
    ),
  ),
  'nocache_hash' => '19466603785638f47592a590-31955474',
  'function' => 
  array (
  ),
  'variables' => 
  array (
    'titulo' => 0,
    '_params' => 0,
    'loc_imagen' => 0,
    'resumen' => 0,
    'contenido' => 0,
    'autor' => 0,
    'date' => 0,
  ),
  'has_nocache_code' => false,
  'version' => 'Smarty-3.1.8',
  'unifunc' => 'content_5638f475971069_54113739',
),false); /*/%%SmartyHeaderCode%%*/?>
<?php if ($_valid && !is_callable('content_5638f475971069_54113739')) {function content_5638f475971069_54113739($_smarty_tpl) {?><header class="col-xs-12 col-sm-12 col-md-12 col-lg-12 clear header">
    <div class="col-xs-12 col-sm-12 col-md-10 col-lg-10 col-md-offset-1 col-lg-offset-1" style="padding: 0px;">
    <div class="container noticia">
    	<div class="row">
    		<div class="col-xs-12 col-sm-12">
    			<h1><?php echo $_smarty_tpl->tpl_vars['titulo']->value;?>
</h1>
    		</div>
    	</div>
        <hr/>
    	<div class="row">
    		<div class="col-xs-8
    					col-sm-8">
    			<img src="<?php echo ($_smarty_tpl->tpl_vars['_params']->value['site']).($_smarty_tpl->tpl_vars['loc_imagen']->value);?>
" class="img-responsive" style="width: 100%">
    		</div>
    	</div>
        <div class="row">
            <div class="col-xs-8
                        col-sm-8">
                <p id="contenido" style="font-style: italic; font-weight: ">
                    <?php echo $_smarty_tpl->tpl_vars['resumen']->value;?>

                </p>
            </div>
        </div>
    	<div class="row">
    		<div class="col-xs-8
    					col-sm-8">
				<p id="contenido">
					<?php echo $_smarty_tpl->tpl_vars['contenido']->value;?>

				</p>

    		</div>
    	</div>
        <br><br>
    	<div class="row">
	    	<div class="col-xs-12
	    				col-sm-12">
	    		<blockquote >
	    			<?php echo $_smarty_tpl->tpl_vars['autor']->value;?>
 <cite><?php echo $_smarty_tpl->tpl_vars['date']->value;?>
</cite>
	    		</blockquote>
	    	</div>
    	</div>
        <br><br><br>
    </div>
    </div>
</header><?php }} ?>