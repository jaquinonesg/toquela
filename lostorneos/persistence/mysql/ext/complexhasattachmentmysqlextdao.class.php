<?php

/**
 * Class that operate on table 'complex_has_attachment'. Database Mysql.
 *
 * @author: http://phpdao.com
 * @date: 2013-07-17 11:18
 */
class ComplexHasAttachmentMySqlExtDAO extends ComplexHasAttachmentMySqlDAO {

    public function deleteByComplex($cod_complex) {
        $sql = "DELETE FROM lostorneos.complex_has_attachment WHERE cod_complex  = '$cod_complex';";
        return $this->executeUpdate($sql);
    }

    public function hasReserve($cod_complex) {
        $sql = "SELECT COUNT(*) AS 'total' FROM lostorneos.reserve r
                    WHERE r.cod_sub_complex IN (SELECT cod_sub_complex FROM lostorneos.sub_complex WHERE cod_complex = '$cod_complex')";
        $object = $this->getRow($sql, true);
        return $object->total;
    }

    public function getCountAll() {
        $sql = "SELECT COUNT(*) as total FROM complex_has_attachment ";
        return $this->getValue($sql);
    }
    
     public function getBySubComplex($value) {
        $this->set($value);
        $sql = "SELECT * FROM lostorneos.complex_has_attachment WHERE cod_sub_complex = '$value'";
        return $this->getRow($sql, true);
    }

}

?>
