<?php 
 /**
 * Intreface DAO
 *
 * @author: http://phpdao.com
 * @date: 2015-11-23 17:10
 */
interface AditionalDAO{

	/**
	 * Get Domain object by primry key
	 *
	 * @param String $id primary key
	 * @Return AditionalMySql 
	 */
	public function load($coduser);

	/**
	 * Get all records from table
	 */
	public function queryAll();
	
	/**
	 * Get all records from table ordered by field
	 * @Param $orderColumn column name
	 */
	public function queryAllOrderBy($orderColumn);
	
	/**
 	 * Delete record from table
 	 * @param aditional primary key
 	 */
	public function delete($coduser);
	
	/**
 	 * Insert record to table
 	 *
 	 * @param AditionalMySql aditional
 	 */
	public function insert($aditional);
	
	/**
 	 * Update record in table
 	 *
 	 * @param AditionalMySql aditional
 	 */
	public function update($aditional);	

	/**
	 * Delete all rows
	 */
	public function clean();

      public function queryByTypedoc($value);    
       public function queryByNumdoc($value);    
       public function queryByCondition($value);    
       public function queryByCivilState($value);    
       public function queryByBirthPlace($value);    
       public function queryByDateBirth($value);    
       public function queryByNationality($value);    
       public function queryByAcademicPreparation($value);    
       public function queryByUniversity($value);    
       public function queryByProfession($value);    
       public function queryByEconomicActivity($value);    
       public function queryByEnterprise($value);    
       public function queryByTeamcarnet($value);    
       public function queryByFicha($value);    
       public function queryByPosition($value);    
       public function queryByAddress($value);    
       public function queryByHousePhone($value);    
       public function queryByTournaments($value);    
       public function queryByPeriod($value);    
       public function queryByTeams($value);    
       public function queryByProfessionalPlay($value);    
       public function queryByHowLong($value);    
       public function queryBySocialClub($value);    
       public function queryByPartnerNumber($value);    
       public function queryByEps($value);    
       public function queryByPrepaidMedicine($value);    
       public function queryByCategory($value);    
       public function queryByTypeblood($value);    
       public function queryByGuardian($value);    
    

        public function deleteByTypedoc($value);    
        public function deleteByNumdoc($value);    
        public function deleteByCondition($value);    
        public function deleteByCivilState($value);    
        public function deleteByBirthPlace($value);    
        public function deleteByDateBirth($value);    
        public function deleteByNationality($value);    
        public function deleteByAcademicPreparation($value);    
        public function deleteByUniversity($value);    
        public function deleteByProfession($value);    
        public function deleteByEconomicActivity($value);    
        public function deleteByEnterprise($value);    
        public function deleteByTeamcarnet($value);    
        public function deleteByFicha($value);    
        public function deleteByPosition($value);    
        public function deleteByAddress($value);    
        public function deleteByHousePhone($value);    
        public function deleteByTournaments($value);    
        public function deleteByPeriod($value);    
        public function deleteByTeams($value);    
        public function deleteByProfessionalPlay($value);    
        public function deleteByHowLong($value);    
        public function deleteBySocialClub($value);    
        public function deleteByPartnerNumber($value);    
        public function deleteByEps($value);    
        public function deleteByPrepaidMedicine($value);    
        public function deleteByCategory($value);    
        public function deleteByTypeblood($value);    
        public function deleteByGuardian($value);    
    
    /**
    * Delete all rows
    */
    public function describe();
    
}

?>