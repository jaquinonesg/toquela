<?php
 /**
 * Class that operate on table 'aditional'. Database Mysql.
 *
 * @author: http://phpdao.com
 * @date: 2014-01-22 16:53
 */
 class AditionalMySqlExtDAO extends AditionalMySqlDAO{

    public function getByPage($page, $limit = 10) {
        $this->set($page);
        $this->set($limit);
        $page = abs((int) $page);
        if (!preg_match('!^\d+$!', $limit)) {
            throw new ErrorException('limit deberia ser un entero');
            return false;
        }
        $limit = abs($limit);

        $sql = "SELECT * FROM interclubes.aditional LIMIT $page , $limit";
        return $this->getList($sql);
    }

    public function getCountAll() {
        $sql = "SELECT COUNT(*) as total FROM interclubes.aditional ";
        return $this->getValue($sql);
    }

    public function insertRecord($cod_user){
        $sql = "INSERT INTO `interclubes`.`aditional` (`cod_user`, `typedoc`, `numdoc`, `datebirth`, `category`, `typeblood`, `eps`, `profession`, `university`, `nationality`) VALUES ('$cod_user', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);";
        return $this->executeInsert($sql);
    }

    public function insertWithValsNulls($aditional) {
         $this->set($aditional->typedoc );
         $this->set($aditional->numdoc );
         $this->set($aditional->condition );
         $this->set($aditional->civilState );
         $this->set($aditional->birthPlace );
         $this->set($aditional->dateBirth );
         $this->set($aditional->nationality );
         $this->set($aditional->academicPreparation );
         $this->set($aditional->university );
         $this->set($aditional->profession );
         $this->set($aditional->economicActivity );
         $this->set($aditional->enterprise );
         $this->set($aditional->teamcarnet );
         $this->set($aditional->ficha );
         $this->set($aditional->position );
         $this->set($aditional->address );
         $this->set($aditional->housePhone );
         $this->set($aditional->tournaments );
         $this->set($aditional->period );
         $this->set($aditional->teams );
         $this->set($aditional->professionalPlay );
         $this->set($aditional->howLong );
         $this->set($aditional->socialClub );
         $this->set($aditional->partnerNumber );
         $this->set($aditional->eps );
         $this->set($aditional->prepaidMedicine );
         $this->set($aditional->category );
         $this->set($aditional->typeblood );
         $this->set($aditional->guardian );

         echo "<pre>";
         @print_r($aditional);
         echo "</pre>";
         echo "<hr />";
         exit();
        $sql = "INSERT INTO interclubes.aditional ( cod_user, typedoc, numdoc, aditional.condition, civilState, birthPlace, dateBirth, nationality, academicPreparation, university, profession, economicActivity, enterprise, aditional.position, address, housePhone, tournaments, period, teams, professionalPlay, howLong, socialClub, partnerNumber, eps, prepaidMedicine, category, typeblood, guardian, teamcarnet)
        VALUES ('$aditional->coduser','$aditional->typedoc','$aditional->numdoc','$aditional->condition','$aditional->civilState','$aditional->birthPlace','$aditional->dateBirth','$aditional->nationality','$aditional->academicPreparation','$aditional->university','$aditional->profession','$aditional->economicActivity','$aditional->enterprise','$aditional->position','$aditional->address','$aditional->housePhone','$aditional->tournaments','$aditional->period','$aditional->teams','$aditional->professionalPlay','$aditional->howLong','$aditional->socialClub','$aditional->partnerNumber','$aditional->eps','$aditional->prepaidMedicine','$aditional->category','$aditional->typeblood','$aditional->guardian',NULL)";
        $id = $this->executeInsert($sql);
        return $id;
    }

    /**
     * Devuelve los campos de aditional cuando el argumento $cod corresponde a cod_user,
     * se supone que cod siempre es un número
     *
     * @author Jorge Quiñones
     * @param  int $cod             Código del usuario
     * @return Object[Aditional]    Objeto de la clase aditional
     */
    public function queryByCoduser($cod){
        $sql = " SELECT * FROM interclubes.aditional WHERE cod_user = $cod ;";
        return $this->getRow($sql);
    }


}

?>