<?php 
 /**
 * Clase que opera sobre la tabla 'aditional'. Database Mysql.
 *
 * @author: Hernán Cortés <heralfstb@gmail.com>
 * @date: 2015-11-23 17:10
 */
class AditionalMySqlDAO extends ModelDAO implements AditionalDAO{

    /**
     * Obtiene el registro por medio de las llaves primarias
     *
     * @return Aditional
     */
    public function load($coduser){
        
$this->set($coduser);
        $sql = "SELECT * FROM interclubes.aditional WHERE  cod_user =  '$coduser'";
        return $this->getRow($sql);
    }

    /**
     * Obtiene todo los registro de la tabla
     */
    /**
     * Obtener todos los registro de las tablas
     */
    public function queryAll($limit = null, $page = null) {
        $extra = "";
        if (!is_null($page)) {
            $this->set($page);
            $this->set($limit);
            $page = abs((int) $page);
            if (!preg_match('!^\d+$!', $limit)) {
                throw new ErrorException('limit deberia ser un entero');
                return false;
            }
            if (!preg_match('!^\d+$!', $page)) {
                throw new ErrorException('limit deberia ser un entero');
                return false;
            }
            $limit = abs($limit);
            $extra = "  LIMIT $page , $limit";
        } elseif (!is_null($limit)) {
            if (!preg_match('!^\d+$!', $limit)) {
                throw new ErrorException('limit deberia ser un entero');
                return false;
            }
            $extra = " LIMIT $limit";
        }
        $sql = "SELECT * FROM interclubes.aditional $extra";
        return $this->getList($sql);
    }
	
    /**
     * Obtiene los registros de la tabla ordenados por un campo en espcifico
     *
     * @param $orderColumn column name
     */
    public function queryAllOrderBy($orderColumn){
        $sql = "SELECT * FROM interclubes.aditional ORDER BY $orderColumn";
        return $this->getList($sql);
    }
	
    /**
     * Borra un registro de la tabla segun las llaves primarias
     * @param aditional primary key
     */
    public function delete($coduser){
            
$this->set($coduser);
            $sql = "DELETE FROM interclubes.aditional WHERE  cod_user =  '$coduser'";
            return $this->executeUpdate($sql);
    }
	
    /**
     * Insert record to table
     *
     * @param Aditional aditional
     */
    public function insert($aditional){
            $this->set($aditional->typedoc);
$this->set($aditional->numdoc);
$this->set($aditional->condition);
$this->set($aditional->civilState);
$this->set($aditional->birthPlace);
$this->set($aditional->dateBirth);
$this->set($aditional->nationality);
$this->set($aditional->academicPreparation);
$this->set($aditional->university);
$this->set($aditional->profession);
$this->set($aditional->economicActivity);
$this->set($aditional->enterprise);
$this->set($aditional->teamcarnet);
$this->set($aditional->ficha);
$this->set($aditional->position);
$this->set($aditional->address);
$this->set($aditional->housePhone);
$this->set($aditional->tournaments);
$this->set($aditional->period);
$this->set($aditional->teams);
$this->set($aditional->professionalPlay);
$this->set($aditional->howLong);
$this->set($aditional->socialClub);
$this->set($aditional->partnerNumber);
$this->set($aditional->eps);
$this->set($aditional->prepaidMedicine);
$this->set($aditional->category);
$this->set($aditional->typeblood);
$this->set($aditional->guardian);
            
            $sql = "INSERT INTO interclubes.aditional ( cod_user , typedoc , numdoc , condition , civilState , birthPlace , dateBirth , nationality , academicPreparation , university , profession , economicActivity , enterprise , teamcarnet , ficha , position , address , housePhone , tournaments , period , teams , professionalPlay , howLong , socialClub , partnerNumber , eps , prepaidMedicine , category , typeblood , guardian ) 
                    VALUES ('$aditional->coduser','$aditional->typedoc','$aditional->numdoc','$aditional->condition','$aditional->civilState','$aditional->birthPlace','$aditional->dateBirth','$aditional->nationality','$aditional->academicPreparation','$aditional->university','$aditional->profession','$aditional->economicActivity','$aditional->enterprise','$aditional->teamcarnet','$aditional->ficha','$aditional->position','$aditional->address','$aditional->housePhone','$aditional->tournaments','$aditional->period','$aditional->teams','$aditional->professionalPlay','$aditional->howLong','$aditional->socialClub','$aditional->partnerNumber','$aditional->eps','$aditional->prepaidMedicine','$aditional->category','$aditional->typeblood','$aditional->guardian')";
            $id = $this->executeInsert($sql);	
            /*$aditional-> = $id;*/
            return $id;
    }
	
    /**
     * Update record in table
     *
     * @param Aditional aditional
     */
    public function update($aditional){
        $this->set($aditional->typedoc);
$this->set($aditional->numdoc);
$this->set($aditional->condition);
$this->set($aditional->civilState);
$this->set($aditional->birthPlace);
$this->set($aditional->dateBirth);
$this->set($aditional->nationality);
$this->set($aditional->academicPreparation);
$this->set($aditional->university);
$this->set($aditional->profession);
$this->set($aditional->economicActivity);
$this->set($aditional->enterprise);
$this->set($aditional->teamcarnet);
$this->set($aditional->ficha);
$this->set($aditional->position);
$this->set($aditional->address);
$this->set($aditional->housePhone);
$this->set($aditional->tournaments);
$this->set($aditional->period);
$this->set($aditional->teams);
$this->set($aditional->professionalPlay);
$this->set($aditional->howLong);
$this->set($aditional->socialClub);
$this->set($aditional->partnerNumber);
$this->set($aditional->eps);
$this->set($aditional->prepaidMedicine);
$this->set($aditional->category);
$this->set($aditional->typeblood);
$this->set($aditional->guardian);
        
        $sql = "UPDATE interclubes.aditional  SET 
		 typedoc = if( strcmp('$aditional->typedoc'  , '' ) = 1  , '$aditional->typedoc', typedoc ),
		 numdoc = if( strcmp('$aditional->numdoc'  , '' ) = 1  , '$aditional->numdoc', numdoc ),
		 condition = if( strcmp('$aditional->condition'  , '' ) = 1  , '$aditional->condition', condition ),
		 civilState = if( strcmp('$aditional->civilState'  , '' ) = 1  , '$aditional->civilState', civilState ),
		 birthPlace = if( strcmp('$aditional->birthPlace'  , '' ) = 1  , '$aditional->birthPlace', birthPlace ),
		 dateBirth = if( strcmp('$aditional->dateBirth'  , '' ) = 1  , '$aditional->dateBirth', dateBirth ),
		 nationality = if( strcmp('$aditional->nationality'  , '' ) = 1  , '$aditional->nationality', nationality ),
		 academicPreparation = if( strcmp('$aditional->academicPreparation'  , '' ) = 1  , '$aditional->academicPreparation', academicPreparation ),
		 university = if( strcmp('$aditional->university'  , '' ) = 1  , '$aditional->university', university ),
		 profession = if( strcmp('$aditional->profession'  , '' ) = 1  , '$aditional->profession', profession ),
		 economicActivity = if( strcmp('$aditional->economicActivity'  , '' ) = 1  , '$aditional->economicActivity', economicActivity ),
		 enterprise = if( strcmp('$aditional->enterprise'  , '' ) = 1  , '$aditional->enterprise', enterprise ),
		 teamcarnet = if( strcmp('$aditional->teamcarnet'  , '' ) = 1  , '$aditional->teamcarnet', teamcarnet ),
		 ficha = if( strcmp('$aditional->ficha'  , '' ) = 1  , '$aditional->ficha', ficha ),
		 position = if( strcmp('$aditional->position'  , '' ) = 1  , '$aditional->position', position ),
		 address = if( strcmp('$aditional->address'  , '' ) = 1  , '$aditional->address', address ),
		 housePhone = if( strcmp('$aditional->housePhone'  , '' ) = 1  , '$aditional->housePhone', housePhone ),
		 tournaments = if( strcmp('$aditional->tournaments'  , '' ) = 1  , '$aditional->tournaments', tournaments ),
		 period = if( strcmp('$aditional->period'  , '' ) = 1  , '$aditional->period', period ),
		 teams = if( strcmp('$aditional->teams'  , '' ) = 1  , '$aditional->teams', teams ),
		 professionalPlay = if( strcmp('$aditional->professionalPlay'  , '' ) = 1  , '$aditional->professionalPlay', professionalPlay ),
		 howLong = if( strcmp('$aditional->howLong'  , '' ) = 1  , '$aditional->howLong', howLong ),
		 socialClub = if( strcmp('$aditional->socialClub'  , '' ) = 1  , '$aditional->socialClub', socialClub ),
		 partnerNumber = if( strcmp('$aditional->partnerNumber'  , '' ) = 1  , '$aditional->partnerNumber', partnerNumber ),
		 eps = if( strcmp('$aditional->eps'  , '' ) = 1  , '$aditional->eps', eps ),
		 prepaidMedicine = if( strcmp('$aditional->prepaidMedicine'  , '' ) = 1  , '$aditional->prepaidMedicine', prepaidMedicine ),
		 category = if( strcmp('$aditional->category'  , '' ) = 1  , '$aditional->category', category ),
		 typeblood = if( strcmp('$aditional->typeblood'  , '' ) = 1  , '$aditional->typeblood', typeblood ),
		 guardian = if( strcmp('$aditional->guardian'  , '' ) = 1  , '$aditional->guardian', guardian ) WHERE  cod_user =  '$aditional->coduser'";
        return $this->executeUpdate($sql);
    }

    /**
     * Delete all rows
     */
    public function clean(){
        $sql = 'DELETE FROM interclubes.aditional';
        return $this->executeUpdate($sql);
    }


                        public function queryByTypedoc($value) {
        $this->set($value);
        $sql = "SELECT * FROM interclubes.aditional WHERE typedoc  = '$value'";
        return $this->getList($sql);    
    }
    
                    public function queryByNumdoc($value) {
        $this->set($value);
        $sql = "SELECT * FROM interclubes.aditional WHERE numdoc  = '$value'";
        return $this->getList($sql);    
    }
    
                    public function queryByCondition($value) {
        $this->set($value);
        $sql = "SELECT * FROM interclubes.aditional WHERE condition  = '$value'";
        return $this->getList($sql);    
    }
    
                    public function queryByCivilState($value) {
        $this->set($value);
        $sql = "SELECT * FROM interclubes.aditional WHERE civilState  = '$value'";
        return $this->getList($sql);    
    }
    
                    public function queryByBirthPlace($value) {
        $this->set($value);
        $sql = "SELECT * FROM interclubes.aditional WHERE birthPlace  = '$value'";
        return $this->getList($sql);    
    }
    
                    public function queryByDateBirth($value) {
        $this->set($value);
        $sql = "SELECT * FROM interclubes.aditional WHERE dateBirth  = '$value'";
        return $this->getList($sql);    
    }
    
                    public function queryByNationality($value) {
        $this->set($value);
        $sql = "SELECT * FROM interclubes.aditional WHERE nationality  = '$value'";
        return $this->getList($sql);    
    }
    
                    public function queryByAcademicPreparation($value) {
        $this->set($value);
        $sql = "SELECT * FROM interclubes.aditional WHERE academicPreparation  = '$value'";
        return $this->getList($sql);    
    }
    
                    public function queryByUniversity($value) {
        $this->set($value);
        $sql = "SELECT * FROM interclubes.aditional WHERE university  = '$value'";
        return $this->getList($sql);    
    }
    
                    public function queryByProfession($value) {
        $this->set($value);
        $sql = "SELECT * FROM interclubes.aditional WHERE profession  = '$value'";
        return $this->getList($sql);    
    }
    
                    public function queryByEconomicActivity($value) {
        $this->set($value);
        $sql = "SELECT * FROM interclubes.aditional WHERE economicActivity  = '$value'";
        return $this->getList($sql);    
    }
    
                    public function queryByEnterprise($value) {
        $this->set($value);
        $sql = "SELECT * FROM interclubes.aditional WHERE enterprise  = '$value'";
        return $this->getList($sql);    
    }
    
                    public function queryByTeamcarnet($value) {
        $this->set($value);
        $sql = "SELECT * FROM interclubes.aditional WHERE teamcarnet  = '$value'";
        return $this->getList($sql);    
    }
    
                    public function queryByFicha($value) {
        $this->set($value);
        $sql = "SELECT * FROM interclubes.aditional WHERE ficha  = '$value'";
        return $this->getList($sql);    
    }
    
                    public function queryByPosition($value) {
        $this->set($value);
        $sql = "SELECT * FROM interclubes.aditional WHERE position  = '$value'";
        return $this->getList($sql);    
    }
    
                    public function queryByAddress($value) {
        $this->set($value);
        $sql = "SELECT * FROM interclubes.aditional WHERE address  = '$value'";
        return $this->getList($sql);    
    }
    
                    public function queryByHousePhone($value) {
        $this->set($value);
        $sql = "SELECT * FROM interclubes.aditional WHERE housePhone  = '$value'";
        return $this->getList($sql);    
    }
    
                    public function queryByTournaments($value) {
        $this->set($value);
        $sql = "SELECT * FROM interclubes.aditional WHERE tournaments  = '$value'";
        return $this->getList($sql);    
    }
    
                    public function queryByPeriod($value) {
        $this->set($value);
        $sql = "SELECT * FROM interclubes.aditional WHERE period  = '$value'";
        return $this->getList($sql);    
    }
    
                    public function queryByTeams($value) {
        $this->set($value);
        $sql = "SELECT * FROM interclubes.aditional WHERE teams  = '$value'";
        return $this->getList($sql);    
    }
    
                    public function queryByProfessionalPlay($value) {
        $this->set($value);
        $sql = "SELECT * FROM interclubes.aditional WHERE professionalPlay  = '$value'";
        return $this->getList($sql);    
    }
    
                    public function queryByHowLong($value) {
        $this->set($value);
        $sql = "SELECT * FROM interclubes.aditional WHERE howLong  = '$value'";
        return $this->getList($sql);    
    }
    
                    public function queryBySocialClub($value) {
        $this->set($value);
        $sql = "SELECT * FROM interclubes.aditional WHERE socialClub  = '$value'";
        return $this->getList($sql);    
    }
    
                    public function queryByPartnerNumber($value) {
        $this->set($value);
        $sql = "SELECT * FROM interclubes.aditional WHERE partnerNumber  = '$value'";
        return $this->getList($sql);    
    }
    
                    public function queryByEps($value) {
        $this->set($value);
        $sql = "SELECT * FROM interclubes.aditional WHERE eps  = '$value'";
        return $this->getList($sql);    
    }
    
                    public function queryByPrepaidMedicine($value) {
        $this->set($value);
        $sql = "SELECT * FROM interclubes.aditional WHERE prepaidMedicine  = '$value'";
        return $this->getList($sql);    
    }
    
                    public function queryByCategory($value) {
        $this->set($value);
        $sql = "SELECT * FROM interclubes.aditional WHERE category  = '$value'";
        return $this->getList($sql);    
    }
    
                    public function queryByTypeblood($value) {
        $this->set($value);
        $sql = "SELECT * FROM interclubes.aditional WHERE typeblood  = '$value'";
        return $this->getList($sql);    
    }
    
                    public function queryByGuardian($value) {
        $this->set($value);
        $sql = "SELECT * FROM interclubes.aditional WHERE guardian  = '$value'";
        return $this->getList($sql);    
    }
    
                
                 
            public function deleteByTypedoc($value) {
        $this->set($value);
        $sql = "DELETE FROM interclubes.aditional WHERE typedoc  = '$value'";
        return $this->executeUpdate($sql);
        }
        
            
            public function deleteByNumdoc($value) {
        $this->set($value);
        $sql = "DELETE FROM interclubes.aditional WHERE numdoc  = '$value'";
        return $this->executeUpdate($sql);
        }
        
            
            public function deleteByCondition($value) {
        $this->set($value);
        $sql = "DELETE FROM interclubes.aditional WHERE condition  = '$value'";
        return $this->executeUpdate($sql);
        }
        
            
            public function deleteByCivilState($value) {
        $this->set($value);
        $sql = "DELETE FROM interclubes.aditional WHERE civilState  = '$value'";
        return $this->executeUpdate($sql);
        }
        
            
            public function deleteByBirthPlace($value) {
        $this->set($value);
        $sql = "DELETE FROM interclubes.aditional WHERE birthPlace  = '$value'";
        return $this->executeUpdate($sql);
        }
        
            
            public function deleteByDateBirth($value) {
        $this->set($value);
        $sql = "DELETE FROM interclubes.aditional WHERE dateBirth  = '$value'";
        return $this->executeUpdate($sql);
        }
        
            
            public function deleteByNationality($value) {
        $this->set($value);
        $sql = "DELETE FROM interclubes.aditional WHERE nationality  = '$value'";
        return $this->executeUpdate($sql);
        }
        
            
            public function deleteByAcademicPreparation($value) {
        $this->set($value);
        $sql = "DELETE FROM interclubes.aditional WHERE academicPreparation  = '$value'";
        return $this->executeUpdate($sql);
        }
        
            
            public function deleteByUniversity($value) {
        $this->set($value);
        $sql = "DELETE FROM interclubes.aditional WHERE university  = '$value'";
        return $this->executeUpdate($sql);
        }
        
            
            public function deleteByProfession($value) {
        $this->set($value);
        $sql = "DELETE FROM interclubes.aditional WHERE profession  = '$value'";
        return $this->executeUpdate($sql);
        }
        
            
            public function deleteByEconomicActivity($value) {
        $this->set($value);
        $sql = "DELETE FROM interclubes.aditional WHERE economicActivity  = '$value'";
        return $this->executeUpdate($sql);
        }
        
            
            public function deleteByEnterprise($value) {
        $this->set($value);
        $sql = "DELETE FROM interclubes.aditional WHERE enterprise  = '$value'";
        return $this->executeUpdate($sql);
        }
        
            
            public function deleteByTeamcarnet($value) {
        $this->set($value);
        $sql = "DELETE FROM interclubes.aditional WHERE teamcarnet  = '$value'";
        return $this->executeUpdate($sql);
        }
        
            
            public function deleteByFicha($value) {
        $this->set($value);
        $sql = "DELETE FROM interclubes.aditional WHERE ficha  = '$value'";
        return $this->executeUpdate($sql);
        }
        
            
            public function deleteByPosition($value) {
        $this->set($value);
        $sql = "DELETE FROM interclubes.aditional WHERE position  = '$value'";
        return $this->executeUpdate($sql);
        }
        
            
            public function deleteByAddress($value) {
        $this->set($value);
        $sql = "DELETE FROM interclubes.aditional WHERE address  = '$value'";
        return $this->executeUpdate($sql);
        }
        
            
            public function deleteByHousePhone($value) {
        $this->set($value);
        $sql = "DELETE FROM interclubes.aditional WHERE housePhone  = '$value'";
        return $this->executeUpdate($sql);
        }
        
            
            public function deleteByTournaments($value) {
        $this->set($value);
        $sql = "DELETE FROM interclubes.aditional WHERE tournaments  = '$value'";
        return $this->executeUpdate($sql);
        }
        
            
            public function deleteByPeriod($value) {
        $this->set($value);
        $sql = "DELETE FROM interclubes.aditional WHERE period  = '$value'";
        return $this->executeUpdate($sql);
        }
        
            
            public function deleteByTeams($value) {
        $this->set($value);
        $sql = "DELETE FROM interclubes.aditional WHERE teams  = '$value'";
        return $this->executeUpdate($sql);
        }
        
            
            public function deleteByProfessionalPlay($value) {
        $this->set($value);
        $sql = "DELETE FROM interclubes.aditional WHERE professionalPlay  = '$value'";
        return $this->executeUpdate($sql);
        }
        
            
            public function deleteByHowLong($value) {
        $this->set($value);
        $sql = "DELETE FROM interclubes.aditional WHERE howLong  = '$value'";
        return $this->executeUpdate($sql);
        }
        
            
            public function deleteBySocialClub($value) {
        $this->set($value);
        $sql = "DELETE FROM interclubes.aditional WHERE socialClub  = '$value'";
        return $this->executeUpdate($sql);
        }
        
            
            public function deleteByPartnerNumber($value) {
        $this->set($value);
        $sql = "DELETE FROM interclubes.aditional WHERE partnerNumber  = '$value'";
        return $this->executeUpdate($sql);
        }
        
            
            public function deleteByEps($value) {
        $this->set($value);
        $sql = "DELETE FROM interclubes.aditional WHERE eps  = '$value'";
        return $this->executeUpdate($sql);
        }
        
            
            public function deleteByPrepaidMedicine($value) {
        $this->set($value);
        $sql = "DELETE FROM interclubes.aditional WHERE prepaidMedicine  = '$value'";
        return $this->executeUpdate($sql);
        }
        
            
            public function deleteByCategory($value) {
        $this->set($value);
        $sql = "DELETE FROM interclubes.aditional WHERE category  = '$value'";
        return $this->executeUpdate($sql);
        }
        
            
            public function deleteByTypeblood($value) {
        $this->set($value);
        $sql = "DELETE FROM interclubes.aditional WHERE typeblood  = '$value'";
        return $this->executeUpdate($sql);
        }
        
            
            public function deleteByGuardian($value) {
        $this->set($value);
        $sql = "DELETE FROM interclubes.aditional WHERE guardian  = '$value'";
        return $this->executeUpdate($sql);
        }
        
            
        
	
    /**
     * Read row
     *
     * @return Aditional 
     */
    protected function readRow($row) {
        $aditional = new Aditional();
        $aditional->coduser = $row['cod_user'];
        $aditional->typedoc = $row['typedoc'];
        $aditional->numdoc = $row['numdoc'];
        $aditional->condition = $row['condition'];
        $aditional->civilState = $row['civilState'];
        $aditional->birthPlace = $row['birthPlace'];
        $aditional->dateBirth = $row['dateBirth'];
        $aditional->nationality = $row['nationality'];
        $aditional->academicPreparation = $row['academicPreparation'];
        $aditional->university = $row['university'];
        $aditional->profession = $row['profession'];
        $aditional->economicActivity = $row['economicActivity'];
        $aditional->enterprise = $row['enterprise'];
        $aditional->teamcarnet = $row['teamcarnet'];
        $aditional->ficha = $row['ficha'];
        $aditional->position = $row['position'];
        $aditional->address = $row['address'];
        $aditional->housePhone = $row['housePhone'];
        $aditional->tournaments = $row['tournaments'];
        $aditional->period = $row['period'];
        $aditional->teams = $row['teams'];
        $aditional->professionalPlay = $row['professionalPlay'];
        $aditional->howLong = $row['howLong'];
        $aditional->socialClub = $row['socialClub'];
        $aditional->partnerNumber = $row['partnerNumber'];
        $aditional->eps = $row['eps'];
        $aditional->prepaidMedicine = $row['prepaidMedicine'];
        $aditional->category = $row['category'];
        $aditional->typeblood = $row['typeblood'];
        $aditional->guardian = $row['guardian'];
        return $aditional;
    }    
    
    
    public function describe(){
         $sql = "DESC interclubes.aditional";
         return $this->getList($sql, true);
    }
    
}

?>