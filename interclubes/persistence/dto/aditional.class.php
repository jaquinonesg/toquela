<?php 
 
/**
 * Objeto que representa la tabla 'aditional'
 *
 * @author: Hernán Cortés Navarro
 * 
 * @date: 2015-11-23 17:10	 
 */
class Aditional {
    
    public $coduser;
    public $typedoc;
    public $numdoc;
    public $condition;
    public $civilState;
    public $birthPlace;
    public $dateBirth;
    public $nationality;
    public $academicPreparation;
    public $university;
    public $profession;
    public $economicActivity;
    public $enterprise;
    public $teamcarnet;
    public $ficha;
    public $position;
    public $address;
    public $housePhone;
    public $tournaments;
    public $period;
    public $teams;
    public $professionalPlay;
    public $howLong;
    public $socialClub;
    public $partnerNumber;
    public $eps;
    public $prepaidMedicine;
    public $category;
    public $typeblood;
    public $guardian;
        
    /**
     * Obtiene los datos tanto del post como del get 
     * para guardarlos dentro del objeto
     * 
     */
    public function set() {
        $variable = get_class_vars(__CLASS__);
        foreach ($variable as $key => $value) {
            if (!is_null(Controller::get($key))) {
                $this->$key = Controller::get($key);
            } elseif (!is_null(Controller::post($key))) {
                $this->$key = Controller::post($key);
            } else {
                $this->$key = $value;
            }
        }
    }

}

?>